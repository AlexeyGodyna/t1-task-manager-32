package ru.t1.godyna.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.domain.DataXmlSaveFasterXmlRequest;
import ru.t1.godyna.tm.enumerated.Role;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-xml";

    @Getter
    @NotNull
    private final String description = "Save data in xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}

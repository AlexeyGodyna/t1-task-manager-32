package ru.t1.godyna.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.dto.request.AbstractRequest;
import ru.t1.godyna.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface IOperation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
